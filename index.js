
//[SECTION] Javascript Object Notation (JSON)
	// To convert object to string and transport data more easily/lightweight
	// core JS has a built in JSON object that contains methods for a parsing JSON objects and converting strings into JS object
	// 2 Types:
		// Stringified JSON Object - stringify
		// JSON Object- .Parse Method, means parsed converting strings to original data type
	/*
	Syntax
		{
		"propertyA":"valueA"},
		"propertyB":"valueB"}
		}
	*/


// JS Object
		/*{
			"city" : "Quezon City",
			"province" : "Manila",
			"country" : "Philippines"
		}
*/

// JS Arrays
		/*[
			{
				"city" : "Quezon City",
				"province" : "Manila",
				"country" : "Philippines"
			},
			{
				"city" : "Manila City",
				"province" : "Manila",
				"country" : "Philippines"
			}

			]*/


//[SECTION] JSON Methods
	// JSON methods contains methods for parsing and converting data into stringified JSON



	let batchesArr = 
		[
			{
				batchName: "Batch X"
			},
			{
				batchName: "Batch Y"
			}
		]	

	console.log("This is the original array: ")
	console.log(batchesArr);

			// Stringify method is used to convert Javascript Object/Arrays into a string.
	let stringBatchesArr = JSON.stringify(batchesArr)

	console.log("This is the result of the stringify:")
	console.log(stringBatchesArr)
	console.log(typeof stringBatchesArr)

	let data = JSON.stringify ({
		name: "John",
		address: {
			city: "Manila",
			country: "Philippines",
		}
	})

	console.log(data);

// [SECTION] Use stringify method with variables
	// When information is stored in a variable and is not hard coded into an object that is stringified, we can suplly the value with a variable;

	/*let firstName = prompt("What is your first name?");
	let lastName = prompt("What is your last name?");
	let age = Number(prompt("How young are you?"));
	let address = {
		city: prompt("What city you live in?"),
		country: prompt("Which country does your city address belong?")
	}


	let otherData = JSON.stringify ({
		firstName,lastName,age,address
	})

	console.log(otherData);*/



// [SECTION]Converting stringified JSON to Javascript Objects
	// Objects are common data types used in application because of the complex data structures that can be created out of them.
	// information is commonly sent to application in stringified JSON and then converted back into objects
	// This happens both for sending information to a backend application and sending information back to frontend application


	let objectBatchesArr = JSON.parse(stringBatchesArr);
	console.log("This is the stringify version: ")
	console.log(stringBatchesArr)
	console.log("This is the result after the Parse method")
	console.log(objectBatchesArr);
	console.log(typeof objectBatchesArr);

	console.log(objectBatchesArr[0])

	let stringifiedObject = `{
		"name": "John",
		"age": 31,
		"address": {
			"city" : "Manila City",
			"country" : "Philippines"
		}
	}`;

	console.log(JSON.parse(stringifiedObject));

